### FLAT
The FLAT technique consist in taking an image of a uniform color to get the deformation due to the optical system.

### DARK
The Dark technique consist in taking a photo in the dark with the exact same parameters (like exposure time, ISO, temperature, ...). This technic is used to get the *hot* pixels and the thermal noise of the CCD camera.

### BIAS/OFFSET
The BIAS technique (also known as OFFSET technic) consist in taking a picture in the dark with the quickest shutter time possible to get the preload signal of the system.

### Stacking
The stacking technique consist in the creation of a pile of images, giving a mean image of all the images on the pile. This technique also improve the signal to noise ratio, because the mean of the noise tend to be null.