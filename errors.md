### Optics' defaults
The optics' defaults are aberations like vigneting and light dispersion due to the optical system placed before the CCD camera.

### Gamma beams

### Dark current
The dark current is the thermal noise due to the fact that camera is heating and some pixels can be *hot*. Those hot pixels induce bright artifacts. Those artifacts are represented with blue, green or red pixels on images.

### Sensor heating
The sensor heating is due to the preload signal of the captor. This signal is specific to each sensor and is the answer of the captor to the preload signal

### Photons noise

### Scintillation

### Reading noise

### Digitalization noise

### Images substraction
