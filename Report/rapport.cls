\NeedsTeXFormat{LaTeX2e}

\ProvidesClass{rapport}[10/07/2017 Classe pour cours, V1.0]

%----------------------------------------------------------------------------------------
%	CLASSE DE BASE
%----------------------------------------------------------------------------------------

\LoadClass[a4paper, 10pt]{book}

%----------------------------------------------------------------------------------------
%	PACKAGES
%----------------------------------------------------------------------------------------

\RequirePackage[square]{natbib}
\RequirePackage[french, british]{babel}
\RequirePackage[utf8]{inputenc}
\RequirePackage[T1]{fontenc}
\RequirePackage{amsmath}
\RequirePackage{amsfonts}
\RequirePackage{esint}
\RequirePackage{amssymb}
\RequirePackage{amsthm}
\RequirePackage{xcolor}
\RequirePackage{graphicx}
\RequirePackage[colorlinks=true, linkcolor=black, citecolor=black]{hyperref}
\RequirePackage[top=2.5cm, bottom=2.5cm, left=2cm, right=2cm]{geometry}
\RequirePackage{array}
\RequirePackage{lastpage}
\RequirePackage{fancyhdr}
\RequirePackage[french]{minitoc}
\RequirePackage[Glenn]{fncychap}
\RequirePackage[explicit]{titlesec}
\RequirePackage{tabularx}
\RequirePackage{float}
\RequirePackage{multirow}
\RequirePackage{braket}
\RequirePackage{setspace}
\RequirePackage{lipsum}
\RequirePackage{cases}
\RequirePackage{listings}
\RequirePackage{siunitx}
\RequirePackage{subcaption}
\RequirePackage{physics}
\RequirePackage{isomath}
% \sisetup{inter-units-product=\ensu remath{\cdot}}
\lstset{language=Python,
                basicstyle=\ttfamily,
                keywordstyle=\color{purple}\ttfamily,
                stringstyle=\color{darkgreen}\ttfamily,
                commentstyle=\color{orange}\ttfamily,
}
\RequirePackage[most]{tcolorbox}
\newtcblisting[auto counter]{python}[2][]{
  sharp corners, fonttitle=\bfseries, colframe=black, listing only,
  listing options={basicstyle=\ttfamily,language=Python},
  title=Listing \thetcbcounter: #2, #1}
\setstretch{1.0}
%----------------------------------------------------------------------------------------
%	COULEURS PERSONELLES
%----------------------------------------------------------------------------------------

\definecolor{darkgreen}{RGB}{51,102,0}
\definecolor{lightgray}{RGB}{229,229,229}
\definecolor{magenta}{RGB}{128,0,128}
\definecolor{steelblue}{RGB}{70,130,180}
\definecolor{vertforet}{RGB}{34,139,34}
\definecolor{gold}{RGB}{255,215,0}
\definecolor{grisbleu}{RGB}{255,215,0}

%----------------------------------------------------------------------------------------
%	COMMANDES PERSO
%----------------------------------------------------------------------------------------

\newcommand{\nl}{\par\leavevmode\par}
\renewcommand{\parallel}{\mathbin{\!/\mkern-5mu/\!}}
\newcommand{\HRule}{\rule{\linewidth}{0.5mm}}
\newcommand{\tbox}[2]{\fbox{\parbox{#1cm}{#2}}}
\setcounter{secnumdepth}{3}
\renewcommand{\thesection}{\arabic{section}}
\newcommand\partcontent{}


%----------------------------------------------------------------------------------------
%	EN-TÊTE ET PIEDS DE PAGE
%----------------------------------------------------------------------------------------
\fancypagestyle{body}{
  \fancyhf{}
  \renewcommand{\headrulewidth}{2.0pt}
  \fancyhead[ER]{\thepage}
  \fancyhead[EL]{\rightmark}
  \fancyhead[OL]{\thepage}
  \fancyhead[OR]{\Large{\partcontent}}
  \renewcommand{\footrulewidth}{0pt}
  }

\fancypagestyle{toc}{
  \fancyhf{}
  \renewcommand{\headrulewidth}{0pt}
  \fancyfoot[C]{\thepage}
  \renewcommand{\footrulewidth}{0.5pt}
}


%----------------------------------------------------------------------------------------
%	APPARENCE SECTION, SOUS-SECTION, ...
%----------------------------------------------------------------------------------------
\titleclass{\part}{straight}
\titleformat{\part}[display]
  {\huge\bfseries\centering}{\partname~\thepart}{0pt}{}
  \titleformat{\section}
  {\color{black}\Large\sffamily\bfseries}
  {}
  {0em}
  {{\parbox{\dimexpr\linewidth-2\fboxsep\relax}{\Roman{section}. #1}}}
  []
\titleformat{name=\section,numberless}
  {\color{black}\Large\sffamily\bfseries}
  {}
  {0em}
  {{\parbox{\dimexpr\linewidth-2\fboxsep\relax}{#1}}}
  []
\titleformat{\subsection}
  {\color{black}\sffamily\bfseries}
  {}
  {0.5em}
  {\Roman{section}.\arabic{subsection}. #1}
  []
\titleformat{\subsubsection}
  {\sffamily\small\bfseries}
  {}
  {0.5em}
  {\Roman{section}.\arabic{subsection}.\alph{subsubsection}. #1}
  []
\titleformat{\paragraph}[runin]
  {\sffamily\small\bfseries}
  {}
  {0em}
  {#1}
\titlespacing*{\part}{0pc}{3ex \@plus4pt \@minus3pt}{5pt}
\titlespacing*{\section}{0pc}{3ex \@plus4pt \@minus3pt}{5pt}
\titlespacing*{\subsection}{0pc}{2.5ex \@plus3pt \@minus2pt}{0pt}
\titlespacing*{\subsubsection}{0pc}{2ex \@plus2.5pt \@minus1.5pt}{0pt}
\titlespacing*{\paragraph}{0pc}{1.5ex \@plus2pt \@minus1pt}{10pt}

%----------------------------------------------------------------------------------------
%	PARAMETRES PAR DEFAUT
%----------------------------------------------------------------------------------------
\floatplacement{figure}{h!}

%----------------------------------------------------------------------------------------
% STYLES POUR LES BLOCS DE TYPERS THÉORÈMES
%----------------------------------------------------------------------------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%% question box %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newtheoremstyle{qbox}% % Theorem style name
{5pt}% Space above
{5pt}% Space below
{\normalfont}% % Body font
{}% Indent amount
{\small\bf\sffamily\color{steelblue}}% % Theorem head font
{\;}% Punctuation after theorem head
{0.25em}% Space after theorem head
{\small\sffamily\color{steelblue}\thmname{#1}\nobreakspace\thmnumber{\@ifnotempty{#1}{}\@upn{#2}}% Theorem text (e.g. Theorem 2.1)
\thmnote{\nobreakspace\the\thm@notefont\sffamily\bfseries\color{black}---\nobreakspace#3.}} % Optional theorem note

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% answer box %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newtheoremstyle{abox}% % Theorem style name
{5pt}% Space above
{5pt}% Space below
{\normalfont}% % Body font
{}% Indent amount
{\small\bf\sffamily\color{black}}% % Theorem head font
{\;}% Punctuation after theorem head
{0.25em}% Space after theorem head
{\small\sffamily\color{black}\thmname{#1}\nobreakspace\thmnumber{\@ifnotempty{#1}{}\@upn{#2}}% Theorem text (e.g. Theorem 2.1)
\thmnote{\nobreakspace\the\thm@notefont\sffamily\bfseries\color{black}---\nobreakspace#3.}} % Optional theorem note


\theoremstyle{qbox}
\newtheorem*{question}{Question}
\theoremstyle{abox}
\newtheorem*{reponse}{Réponse}
\newtheorem*{sources}{Source(s)}

\setcounter{secnumdepth}{3}
