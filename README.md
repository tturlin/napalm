# NAPALM

Numerical Analysis and Processing of Astronomical Long exposure Measurement


## Systematic errors and possibles corrections
- [x] Optics' defaults ==> FLAT technique
- [x] Gamma beams ==> Stacking technique
- [x] Dark current ==> DARK technique
- [x] Sensor heating ==> OFFSET technique
- [ ] Photons noise
- [ ] Scintillation
- [ ] Reading noise
- [ ] Digitalization noise
- [ ] Images substraction

A description of thoses errors can be found in [errors.md](errors.md) and a solution to those errors can be found in [algo.md](algo.md).


## Bibliography

[Article about dark, flat and offset](https://pg-astro.fr/astronomie/materiel-et-pratique/traitement-d-images.html)

[Documentation about astropy loading FITS](https://docs.astropy.org/en/stable/io/fits/)

[Documentation about astropy printing image](https://docs.astropy.org/en/stable/generated/examples/io/plot_fits-image.html)

[Deep sky stacker](http://deepskystacker.free.fr/english/)