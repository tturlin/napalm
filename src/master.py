#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# @Date    : 2019-11-14 11:24
# @Author  : Théo TURLIN (theo.turlin.22@gmail.com)
# @Author  : Nicolas BELLEMONT (nico.bellemont@gmail.com)
# @Licence : CC-BY-SA
# @Version : 1.1.0

import numpy as np

def combination_offset(offsets):
    """Return the master offset for a given list of offsets

    Arguments:
        offsets {list(2d array)} -- contain the list of offsets

    Returns:
        2d array -- the master offset
    """
    return np.median(offsets, axis=0)


def combination_dark(master_offset, darks):
    """Return the master dark for a given list of darks

    Arguments:
        master_offset {2d array} -- the master offset
        darks {list(2d array)} -- contain the list of darks

    Returns:
        2d array -- the master dark
    """
    darks -= master_offset

    return np.median(darks, axis=0)


def combination_flat(master_offset, master_dark, flats):
    """Return the master flat for a given list of flats

    Arguments:
        master_offset {2d array} -- the master offset
        master_dark {2d array} -- the master dark
        flats {list(2d array)} -- contain the list of flats

    Returns:
        2d array -- the master flat
    """
    flats -= master_offset

    return np.median(flats, axis=0)
