#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# @Date    : 2019-11-14 14:12
# @Author  : Théo TURLIN (theo.turlin.22@gmail.com)
# @Author  : Nicolas BELLEMONT (nico.bellemont@gmail.com)
# @Licence : CC-BY-SA
# @Version : 0.0.0

import astroalign as aa
import astropy.io.fits as fits
import astropy.visualization as visualization
import cv2
import imageio
import logging as log
import matplotlib.pyplot as plt
import numpy as np
import PIL.Image as Image
import scipy.signal as signal
import sys
from tqdm import trange
from astropy.visualization import make_lupton_rgb


R = fits.getdata("../Results/n7331_R_reslog.fits")
G = fits.getdata("../Results/n7331_G_reslog.fits")
B = fits.getdata("../Results/n7331_B_reslog.fits")

# R, _ = aa.register(R, R)
G, _ = aa.register(G, R)
B, _ = aa.register(B, R)
image = make_lupton_rgb(R, G, B, stretch=0.5)
imageio.imwrite("../Results/test.jpg", image)