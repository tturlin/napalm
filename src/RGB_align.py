#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# @Date    : 2020-01-20 16:55
# @Author  : Nicolas BELLEMONT (nico.bellemont@gmail.com)
# @Licence : CC-BY-SA
# @Version : 0.0.0

import astroalign as aa
import astropy.io.fits as fits
import imageio
import numpy as np
from pathlib import Path
import sys


object_path = str(sys.argv[1])
object_name = object_path.split("/")
object_dir = object_name[-1].split("_")

R_path = Path("../Results/"+object_dir[0]+"/"+object_name[-1]+"_R_reslog.fits")
R = fits.getdata(R_path)
R = R.astype(int)

V_path = Path("../Results/"+object_dir[0]+"/"+object_name[-1]+"_V_reslog.fits")
V = fits.getdata(V_path)
V = V.astype(int)

B_path = Path("../Results/"+object_dir[0]+"/"+object_name[-1]+"_B_reslog.fits")
B = fits.getdata(B_path)
B = B.astype(int)

H_path = Path("../Results/"+object_dir[0]+"/"+object_name[-1]+"_HA_reslog.fits")
H = fits.getdata(H_path)
H = H.astype(int)


V, _ = aa.register(V, R)
hdu = fits.PrimaryHDU(V)
hdu.writeto("../Results/"+object_dir[0]+"/"+object_name[-1]+"_V_reslog.fits", overwrite=True)
imageio.imwrite("../Results/"+object_dir[0]+"/"+object_name[-1]+"_V_reslog.jpg", V)

B, _ = aa.register(B, R)
hdu = fits.PrimaryHDU(B)
hdu.writeto("../Results/"+object_dir[0]+"/"+object_name[-1]+"_B_reslog.fits", overwrite=True)
imageio.imwrite("../Results/"+object_dir[0]+"/"+object_name[-1]+"_B_reslog.jpg", B)

H, _ = aa.register(H, R)
hdu = fits.PrimaryHDU(H)
hdu.writeto("../Results/"+object_dir[0]+"/"+object_name[-1]+"_HA_reslog.fits", overwrite=True)
imageio.imwrite("../Results/"+object_dir[0]+"/"+object_name[-1]+"_HA_reslog.jpg", H)
