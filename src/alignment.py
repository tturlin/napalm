b#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# @Date    : 2019-12-04 15:47
# @Author  : Théo TURLIN (theo.turlin.22@gmail.com)
# @Author  : Nicolas BELLEMONT (nico.bellemont@gmail.com)
# @Licence : CC-BY-SA
# @Version : 0.0.0


import numpy as np
import misc
import astropy.io.fits as fits
import imageio


def find_centers(data):
    data[data < 65536 * 0.1] = 0
    data[data > 65536 * 0.1] = 65536

    list_i = []
    list_j = []
    bary = []

    for i in range(data.shape[0]):
        if sum(data[i]) != 0:
            list_i.appand(i)
            while sum(data[i]) > 0:
                i += 1
            list_i.append(i)

    for i in range(len(list_i[::2])):
        for j in range(data.shape[1]):
            if sum(data[list_i[i]:list_i[i + 1] + 1, j]) != 0:
                list_j.append(j)
                while sum(data[list_i[i]:list_i[i + 1] + 1, j]) > 0:
                    j += 1
                list_j.append(j)

    for k in range(0, len(list_i), 2):
        bx = 0.
        by = 0.
        for i in range(list_i[k], list_i[k + 1] + 1):
            for j in range(list_j[k], list_j[k + 1] + 1):
                bx += i * data[i, j]
                by += j * data[i, j]
        bx /= (list_i[k + 1] + 1 - list_i[k])
        by /= (list_j[k + 1] + 1 - list_j[k])
        bary.append(np.around((bx, by)))

    return bary


data_dir = "../Data/n6946/"
test = fits.getdata(misc.path(data_dir, "light")[0])
bary = find_centers(test)

bary_matrix = np.zeros(test.shape)
bary_matrix[bary] = 1

imageio.imwrite("test.jpg", test)
imageio.imwrite("bary_matrix.jpg", bary_matrix)