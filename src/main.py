#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# @Date    : 2019-11-14 14:12
# @Author  : Théo TURLIN (theo.turlin.22@gmail.com)
# @Author  : Nicolas BELLEMONT (nico.bellemont@gmail.com)
# @Licence : CC-BY-SA
# @Version : 1.3.1

import astroalign as aa
import astropy.io.fits as fits
import bokeh.plotting as plot
import imageio
import logging as log
import numpy as np
from pathlib import Path
import subprocess
import sys
from tqdm import trange

import dfo
import master
import misc
import stacking

log.basicConfig(filename='main.log', level=log.DEBUG, filemode="w")

object_path = str(sys.argv[1])
object_name = object_path.split("/")
object_dir = object_name[-1].split("_")

data_dir = "../Data/" + object_path + "/"

offsets = np.array([fits.getdata(i) for i in misc.path(data_dir, "offset")],
                   dtype=np.float32)
master_offset = master.combination_offset(offsets)
del offsets
hdu = fits.PrimaryHDU(master_offset)
hdu.writeto("../Verification/" + object_dir[0] + "/" + object_name[-1] + "_master_offset.fits", overwrite=True)
print("offset done")

darks = np.array([fits.getdata(i) for i in misc.path(data_dir, "dark")],
                 dtype=np.float32)
master_dark = master.combination_dark(master_offset, darks)
del darks
hdu = fits.PrimaryHDU(master_dark)
hdu.writeto("../Verification/" + object_dir[0] + "/" + object_name[-1] + "_master_dark.fits", overwrite=True)
print("dark done")

flats = np.array([fits.getdata(i) for i in misc.path(data_dir, "flat")],
                 dtype=np.float32)
master_flat = master.combination_flat(master_offset, master_dark, flats)
del flats
hdu = fits.PrimaryHDU(master_flat)
hdu.writeto("../Verification/" + object_dir[0] + "/" + object_name[-1] + "_master_flat.fits", overwrite=True)
print("flat done")


lights = np.array([fits.getdata(i) for i in misc.path(data_dir, "light")],
                  dtype=np.float32)

print("Processing")
lights = dfo.preprocess(lights, master_dark, master_flat, master_offset)
hdu = fits.PrimaryHDU(lights[-1])
hdu.writeto("../Verification/" + object_dir[0] + "/" + object_name[-1] + "_preprocess.fits", overwrite=True)

print("Alignment")
for i in trange(len(lights)):
    try:
        lights[i], _ = aa.register(lights[i], lights[0])
    except:
        print("image {} non alignée".format(i))

stacked = stacking.stack(lights)
# maxi = np.max(stacked)
# c = 65536 / (np.log(1 + maxi))
# stacked[stacked < 0] = 0
# stacked[stacked > 0] = c * np.log(stacked[stacked > 0])
stacked += (-1)*np.sign(np.min(stacked))*abs(np.min(stacked))
stacked /= np.max(stacked)
stacked *= 2**16 - 1
hdu = fits.PrimaryHDU(stacked)
hdu.writeto("../Results/" + object_dir[0] + "/" + object_name[-1] + "_reslog.fits", overwrite=True)
imageio.imwrite("../Results/" + object_dir[0] + "/" + object_name[-1] + "_reslog.jpg", stacked)

ref_path = Path("../Data/n6946/result.fits")
ref = np.float32(fits.getdata(ref_path))

ref += (-1)*np.sign(np.min(ref))*abs(np.min(ref))
ref /= np.max(ref)
ref *= 2**16 - 1
print(np.min(ref), np.max(ref))
print(np.min(stacked), np.max(stacked))
stacked, _ = aa.register(stacked, ref)
stacked = np.array(stacked).flatten()
ref = ref.flatten()

x = np.linspace(0, 65536, 65536)
p = plot.figure()
p.circle(ref, stacked)
p.line(x, x, color="red")
plot.show(p)