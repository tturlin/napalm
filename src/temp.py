import astropy.io.fits as fits
import astropy.visualization as visualization
from copy import copy
import imageio
import logging as log
import matplotlib.pyplot as plt
import numpy as np
import PIL.Image as Image
import scipy.signal as signal
import sys
from tqdm import trange

import dfo
import master
import misc
import stacking
data_dir = "../Data/n6946/"
offsets = np.array([fits.getdata(i) for i in misc.path(data_dir, "offset")],
                   dtype=float)
master_offset = master.combination_offset(offsets)
del offsets
print("offset done")

darks = np.array([fits.getdata(i) for i in misc.path(data_dir, "dark")],
                 dtype=float)
master_dark = master.combination_dark(master_offset, darks)
del darks
print("dark done")

flats = np.array([fits.getdata(i) for i in misc.path(data_dir, "flat")],
                 dtype=float)
master_flat = master.combination_flat(master_offset, master_dark, flats)
del flats
print("flat done")


lights = np.array([fits.getdata(i) for i in misc.path(data_dir, "light")],
                  dtype=float)
print("Processing")
lights = dfo.preprocess(lights, master_dark, master_flat, master_offset)

light_0 = copy(lights[0])
light_1 = copy(lights[-1])
imageio.imwrite("../Results/first_light.jpg", lights[0])
imageio.imwrite("../Results/last_light.jpg", lights[-1])

light_0[light_0 < 65536 * 0.9] = 0
light_0[light_0 > 0] = 65536

light_1[light_1 < 65536 * 0.9] = 0
light_1[light_1 > 0] = 65536


def find_centers(data):
    data[data > 0] = 1

    list_i = []
    list_j = []
    bary = []

    i = 0
    while (i < data.shape[0]):
        if sum(data[i]) != 0:
            list_i.append(i)
            while sum(data[i]) > 0:
                i += 1
            list_i.append(i)
        i += 1

    for i in range(0, len(list_i), 2):
        j = 0
        while j < data.shape[1]:
            if sum(data[list_i[i]:list_i[i + 1] + 1, j]) != 0:
                list_j.append(j)
                while sum(data[list_i[i]:list_i[i + 1] + 1, j]) > 0:
                    j += 1
                list_j.append(j)
            j += 1
    for k in range(0, len(list_i), 2):
        bx = 0.
        by = 0.
        cbx = 0
        cby = 0
        for i in range(list_i[k], list_i[k + 1] + 1):
            for j in range(list_j[k], list_j[k + 1] + 1):
                bx += i * data[i, j]
                by += j * data[i, j]
                if data[i, j] != 0:
                    cbx +=1
                    cby +=1
        bx /= cbx
        by /= cby
        bary.append(np.around([bx, by]).astype(int))

    data[data > 0] = 65536

    return bary


def translation(barry_ref, barry_current, current_img):
    move = bary_current[0] - bary_ref[0]
    moved_img = np.zeros(current_img.shape)
    for i in range(moved_img.shape[0]):
        for j in range(moved_img.shape[1]):
            try:
                moved_img[i, j] = current_img[i + move[0], j + move[1]]
            except:
                pass
    return moved_img


def angle(bary_ref, bary_rot):
    vec_rot = bary_rot[-1] - bary_rot[0]
    vec_ref = bary_ref[-1] - bary_ref[0]

    d_ref = np.linalg.norm(vec_ref)
    d_rot = np.linalg.norm(vec_rot)

    th = np.arccos(np.dot(vec_rot, vec_ref) / (d_rot * d_ref))
    print(th)
    return th

def rotation(theta, current_img, pos_center):
    img_shape = current_img.shape
    new_img = np.zeros(img_shape)

    m_rot = np.array([[np.cos(theta), -np.sin(theta)], [np.sin(theta), np.cos(theta)]])
    print("Rotation")
    print("Matrice rotation")
    print(m_rot)
    for i in trange(img_shape[0]):
        for j in range(img_shape[1]):
            if i != pos_center[0] and j != pos_center[1]:
                try:
                    pos = np.dot(m_rot, np.array([i, j])-pos_center)
                    new_img[int(pos[0]+pos_center[0]), int(pos[1]+pos_center[1])] = current_img[i, j]
                except:
                    pass
    return new_img

bary_ref = find_centers(light_0)
bary_current = find_centers(light_1)

new_img = translation(bary_ref, bary_current, lights[-1])

# print(np.sum(lights[-1])/65536)
imageio.imwrite("../Results/moved.jpg", new_img)

bary_rot = find_centers(new_img)

th = angle(bary_ref, bary_current)

new_img = rotation(th, new_img, bary_ref[0])


# print(np.sum(lights[-1])/65536)
imageio.imwrite("../Results/rotate.jpg", new_img)