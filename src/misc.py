#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# @Date    : 2019-11-14 14:40
# @Author  : Théo TURLIN (theo.turlin.22@gmail.com)
# @Author  : Nicolas BELLEMONT (nico.bellemont@gmail.com)
# @Licence : CC-BY-SA
# @Version : 1.1.0

import logging as log
from pathlib import Path
import os


def path(data_dir, type_):
    """Create a list of path to images

    Arguments:
        data_dir {str} -- path to the global folder containing imges subfolders
        type_ {str} -- type of images that we want to get the path

    Raises:
        TypeError: if the type is not one of the possible ones

    Returns:
        list -- list of path object to images
    """

    data_path2 = []

    if type_ == "flat":
        data_path = os.listdir(data_dir + "Flat/")
        for i, j in enumerate(data_path):
            data_path2.append(Path(data_dir + "Flat/" + j))
    elif type_ == "dark":
        data_path = os.listdir(data_dir + "Dark/")
        for i, j in enumerate(data_path):
            data_path2.append(Path(data_dir + "Dark/" + j))
    elif type_ == "offset":
        data_path = os.listdir(data_dir + "Bias/")
        for i, j in enumerate(data_path):
            data_path2.append(Path(data_dir + "Bias/" + j))
    elif type_ == "light":
        data_path = os.listdir(data_dir + "Light/")
        for j in data_path:
            data_path2.append(Path(data_dir + "Light/" + j))
    elif type_ == "light_r":
        data_path = os.listdir(data_dir + "Light/R/")
        for j in data_path:
            data_path2.append(Path(data_dir + "Light/R/" + j))
    elif type_ == "light_g":
        data_path = os.listdir(data_dir + "Light/G/")
        for j in data_path:
            data_path2.append(Path(data_dir + "Light/G/" + j))
    elif type_ == "light_b":
        data_path = os.listdir(data_dir + "Light/B/")
        for j in data_path:
            data_path2.append(Path(data_dir + "Light/B/" + j))
    else:
        log.error("""Type of data to import not found:
                  No folder containing {} images""".format(type_))
        raise TypeError("Unknown type image {}".format(type_))

    return data_path2
