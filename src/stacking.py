#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# @Date    : 2019-11-07 10:45
# @Author  : Théo TURLIN (theo.turlin.22@gmail.com)
# @Author  : Nicolas BELLEMONT (nico.bellemont@gmail.com)
# @Licence : CC-BY-SA
# @Version : 1.2.1


import numpy as np


def stack(data):
    """ Stack a list of array in order to compute the mean of this array
    Args:
        data {2D array} -- list of matrix

    Returns:
        matrix {2D array} -- image averaged
    """
    size = data[0].shape
    matrix = np.zeros((size[0], size[1]))

    for i in data:
        matrix += i

    matrix /= len(data)
    return matrix
