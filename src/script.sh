#!/bin/bash
PATH_=$PWD
OBJECT="$1"
DIR=$PATH_"/../Data/"$OBJECT"/"
echo $DIR
if [ ! -d $DIR ]
then
    echo "Wrong path"
elif [ -d $DIR$OBJECT"_V" ]
then
    echo "RGB Processing"
    mkdir $PATH_"/../Results/"$OBJECT
    mkdir $PATH_"/../Verification/"$OBJECT
    python3 main.py $OBJECT"/"$OBJECT"_R"
    python3 main.py $OBJECT"/"$OBJECT"_V"
    python3 main.py $OBJECT"/"$OBJECT"_B"
    python3 main.py $OBJECT"/"$OBJECT"_HA"
    python3 RGB_align.py $OBJECT
    stiff $PATH_"/../Results/"$OBJECT"/"$OBJECT"_HA_reslog.fits" $PATH_"/../Results/"$OBJECT"/"$OBJECT"_V_reslog.fits" $PATH_"/../Results/"$OBJECT"/"$OBJECT"_B_reslog.fits" -c config
    mv "stiff.tif" "../Results/"$OBJECT"/"$OBJECT"_HA.tif"
    rm "stiff.xml"


    stiff $PATH_"/../Results/"$OBJECT"/"$OBJECT"_R_reslog.fits" $PATH_"/../Results/"$OBJECT"/"$OBJECT"_V_reslog.fits" $PATH_"/../Results/"$OBJECT"/"$OBJECT"_B_reslog.fits" - config
    mv "stiff.tif" "../Results/"$OBJECT"/"$OBJECT"_RGB.tif"
    rm "stiff.xml"

else
    echo "Monoband Processing"
    python3 main.py $OBJECT
fi
