#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# @Date    : 2019-11-07 11:11
# @Author  : Théo TURLIN (theo.turlin.22@gmail.com)
# @Author  : Nicolas BELLEMONT (nico.bellemont@gmail.com)
# @Licence : CC-BY-SA
# @Version : 1.2.0

import numpy as np
from tqdm import trange


def preprocess(lights_x, master_dark, master_flat, master_offset):
    """Apply master dark, flat and offset on each light images on the band x

    Arguments:
        lights_x {list(2d array)} -- contain the list of light images on band x
        master_dark {2d array} -- the master dark
        master_flat {2d array} -- the master flat
        master_offset {2d array} -- the master offset

    Returns:
        list(2d array) -- list containning the preprocessed lights
    """
    for i in trange(len(lights_x)):
        lights_x[i] -= master_offset
        lights_x[i] -= master_dark
        lights_x[i] /= master_flat

    return lights_x
